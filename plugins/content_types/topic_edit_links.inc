<?php

/**
 * @file
 * Plugin that shows topic editor links.
 */

$plugin = array(
  'title' => t('Topic editor links'),
  'single' => TRUE,
  'icon' => 'icon_term.png',
  'content_types' => array('ns_core_topic_edit_links_content_type'),
  'description' => t('Provides links for topics in this panel page'),
  'required context' => new ctools_context_required(t('Term'), array('term', 'taxonomy_term')),
  'category' => t('Taxonomy term'),
);

function ns_core_topic_edit_links_content_type_render($subtype, $conf, $panel_args, $context) {
  $regions = array();
  $topic = $context->data;
  $vids = taxonomy_vocabulary_get_names();
  $tree = taxonomy_get_tree($vids[$conf['vocabulary']]->vid);
  foreach ($tree as $i => $region) {
    $region = array(
      'name' => 'region-' . $region->tid,
      'tid' => $region->tid,
      'link' => l(t('Edit region'), 'admin/content/topic-editor/' . $topic->tid . '/' . $region->tid),
    );
    $regions[] = $region;
  }
  // We need some javascript to make the region editing in-line.
  drupal_add_js(array('ns_ch_web' => array('term' => $topic->tid, 'regions' => $regions)), 'setting');
  drupal_add_js(drupal_get_path('module', 'ns_core') . '/js/adminregion.js');
  drupal_add_css(drupal_get_path('module', 'ns_core') . '/css/adminregion.css', array('type' => 'file', 'group'=> CSS_THEME));
  return NULL;
}

function ns_core_topic_edit_links_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" topic editor links', array('@s' => $context->identifier));
}

function ns_core_topic_edit_links_content_type_edit_form(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
