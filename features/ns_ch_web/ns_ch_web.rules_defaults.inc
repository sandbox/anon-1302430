<?php
/**
 * @file
 * ns_ch_web.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ns_ch_web_default_rules_configuration() {
  $items = array();
  $items['rules_ns_ch_web_clear_article'] = entity_import('rules_config', '{ "rules_ns_ch_web_clear_article" : {
      "LABEL" : "Clear article cache",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_delete", "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : {
                "ns_ad" : "ns_ad",
                "ns_article" : "ns_article",
                "ns_ch_web_promo" : "ns_ch_web_promo"
              }
            }
          }
        }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : {
                "task:node_view_panel_context:node_view:new-1" : "task:node_view_panel_context:node_view:new-1",
                "task:node_view_panel_context:node_view:new-3" : "task:node_view_panel_context:node_view:new-3",
                "task:node_view_panel_context:node_view:new-4" : "task:node_view_panel_context:node_view:new-4",
                "task:node_view_panel_context:node_view:new-12" : "task:node_view_panel_context:node_view:new-12"
              }
            }
          }
        }
      ]
    }
  }');
  $items['rules_ns_ch_web_clear_article_pub'] = entity_import('rules_config', '{ "rules_ns_ch_web_clear_article_pub" : {
      "LABEL" : "Clear promo cache when articles are published",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_update", "node_delete" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "ns_article" : "ns_article" } }
          }
        },
        { "node_is_published" : { "node" : [ "node" ] } }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : {
                "task:term_view_panel_context:term_view:new-1" : "task:term_view_panel_context:term_view:new-1",
                "task:term_view_panel_context:term_view:new-2" : "task:term_view_panel_context:term_view:new-2",
                "task:term_view_panel_context:term_view:new-6" : "task:term_view_panel_context:term_view:new-6",
                "task:term_view_panel_context:term_view:new-7" : "task:term_view_panel_context:term_view:new-7",
                "task:term_view_panel_context:term_view:new-8" : "task:term_view_panel_context:term_view:new-8"
              }
            }
          }
        }
      ]
    }
  }');
  $items['rules_ns_ch_web_clear_fact'] = entity_import('rules_config', '{ "rules_ns_ch_web_clear_fact" : {
      "LABEL" : "Clear fact cache",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_delete", "node_update" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "ns_fact" : "ns_fact" } } } }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : { "task:node_view_panel_context:node_view:new-1" : "task:node_view_panel_context:node_view:new-1" } } } }
      ]
    }
  }');
  $items['rules_ns_ch_web_clear_footer'] = entity_import('rules_config', '{ "rules_ns_ch_web_clear_footer" : {
      "LABEL" : "Clear footer cache",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_delete", "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "ns_footer_content" : "ns_footer_content" } }
          }
        }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : { "task:site_template_panel_context:site_template:new-8" : "task:site_template_panel_context:site_template:new-8" } } } }
      ]
    }
  }');
  $items['rules_ns_ch_web_clear_promo'] = entity_import('rules_config', '{ "rules_ns_ch_web_clear_promo" : {
      "LABEL" : "Clear promo cache",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_delete", "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "ns_ad" : "ns_ad", "ns_ch_web_promo" : "ns_ch_web_promo" } }
          }
        }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : {
                "task:term_view_panel_context:term_view:new-1" : "task:term_view_panel_context:term_view:new-1",
                "task:term_view_panel_context:term_view:new-2" : "task:term_view_panel_context:term_view:new-2",
                "task:term_view_panel_context:term_view:new-6" : "task:term_view_panel_context:term_view:new-6",
                "task:term_view_panel_context:term_view:new-7" : "task:term_view_panel_context:term_view:new-7",
                "task:term_view_panel_context:term_view:new-8" : "task:term_view_panel_context:term_view:new-8"
              }
            }
          }
        }
      ]
    }
  }');
  $items['rules_ns_ch_web_create_promo'] = entity_import('rules_config', '{ "rules_ns_ch_web_create_promo" : {
      "LABEL" : "ns_ch_web_create_promo",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "crossclone" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "ns_article" : "ns_article" } }
          }
        }
      ],
      "DO" : [
        { "crossclone_node_action" : {
            "USING" : {
              "original_node" : [ "node" ],
              "bundle" : "ns_ch_web_promo",
              "clone" : "2",
              "couple_setting" : "0",
              "delete_setting" : "1"
            },
            "PROVIDE" : { "created_node" : { "created_node" : "Created node" } }
          }
        },
        { "node_publish" : { "node" : [ "created-node" ] } },
        { "entity_save" : { "data" : [ "created-node" ] } }
      ]
    }
  }');
  return $items;
}
