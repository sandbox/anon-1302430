<?php
/**
 * @file
 * ns_ch_web.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_ch_web_defaultconfig_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_ch_web_promo content
  $permissions['create ns_ch_web_promo content'] = array(
    'name' => 'create ns_ch_web_promo content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
      4 => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: manage topic regions
  $permissions['manage topic regions'] = array(
    'name' => 'manage topic regions',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
      4 => 'writer',
    ),
    'module' => 'ns_core',
  );

  // Exported permission: delete any ns_ch_web_promo content
  $permissions['delete any ns_ch_web_promo content'] = array(
    'name' => 'delete any ns_ch_web_promo content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ns_ch_web_promo content
  $permissions['delete own ns_ch_web_promo content'] = array(
    'name' => 'delete own ns_ch_web_promo content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ns_ch_web_promo content
  $permissions['edit any ns_ch_web_promo content'] = array(
    'name' => 'edit any ns_ch_web_promo content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ns_ch_web_promo content
  $permissions['edit own ns_ch_web_promo content'] = array(
    'name' => 'edit own ns_ch_web_promo content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
      4 => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: view own unpublished content
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'administrator',
      1 => 'blogger',
      2 => 'chief editor',
      3 => 'editor',
      4 => 'super user',
      5 => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: view own unpublished content
  $permissions['administer dynamic formatters'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'administrator',
      2 => 'chief editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: Allow Reordering
  $permissions['Allow Reordering'] = array(
    'name' => 'Allow Reordering',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'draggableviews',
  );


  return $permissions;
}
