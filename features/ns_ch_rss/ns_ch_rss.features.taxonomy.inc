<?php
/**
 * @file
 * ns_ch_rss.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ns_ch_rss_taxonomy_default_vocabularies() {
  return array(
    'ns_ch_rss_topic' => array(
      'name' => 'RSS Topic',
      'machine_name' => 'ns_ch_rss_topic',
      'description' => 'Categories for RSS feed',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
