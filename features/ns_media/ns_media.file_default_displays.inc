<?php
/**
 * @file
 * ns_media.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function ns_media_file_default_displays() {
  $export = array();

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_22__file_field_styles_file_ns_styles_grid_22';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['video__file_styles_ns_styles_grid_22__file_field_styles_file_ns_styles_grid_22'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_22__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '430',
    'height' => '270',
    'autoplay' => 0,
  );
  $export['video__file_styles_ns_styles_grid_22__media_youtube_video'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_24__file_field_styles_file_ns_styles_grid_24';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['video__file_styles_ns_styles_grid_24__file_field_styles_file_ns_styles_grid_24'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_24__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '470',
    'height' => '216',
    'autoplay' => 0,
  );
  $export['video__file_styles_ns_styles_grid_24__media_youtube_video'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_34__file_field_styles_file_ns_styles_grid_34';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['video__file_styles_ns_styles_grid_34__file_field_styles_file_ns_styles_grid_34'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_34__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '670',
    'height' => '378',
    'autoplay' => 0,
  );
  $export['video__file_styles_ns_styles_grid_34__media_youtube_video'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_48__file_field_styles_file_ns_styles_grid_48';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['video__file_styles_ns_styles_grid_48__file_field_styles_file_ns_styles_grid_48'] = $file_display;

  $file_display = new stdClass;
  $file_display->api_version = 1;
  $file_display->name = 'video__file_styles_ns_styles_grid_48__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '950',
    'height' => '535',
    'autoplay' => 0,
  );
  $export['video__file_styles_ns_styles_grid_48__media_youtube_video'] = $file_display;

  return $export;
}
