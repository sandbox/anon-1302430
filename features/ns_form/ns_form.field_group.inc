<?php
/**
 * @file
 * ns_form.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ns_form_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_form|node|ns_article|form';
  $field_group->group_name = 'group_ns_form';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Forms',
    'weight' => '9',
    'children' => array(
      0 => 'field_ns_form_ns_article_form',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_form|node|ns_article|form'] = $field_group;

  return $export;
}
