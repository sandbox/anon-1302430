<?php
/**
 * @file
 * ns_ch_blog.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ns_ch_blog_default_rules_configuration() {
  $items = array();
  $items['rules_ns_ch_blog_clear_blog'] = entity_import('rules_config', '{ "rules_ns_ch_blog_clear_blog" : {
      "LABEL" : "Clear blog cache",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "cache_actions" ],
      "ON" : [ "node_insert", "node_delete", "node_update" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "ns_blog" : "ns_blog", "ns_blog_post" : "ns_blog_post" } }
          }
        }
      ],
      "DO" : [
        { "cache_actions_action_clear_panels_pane_cache" : { "panes" : { "value" : {
                "task:site_template_panel_context:site_template:new-4" : "task:site_template_panel_context:site_template:new-4",
                "subtask:page_ns_ch_blog_blog_panel_context:page:ns_ch_blog_blog:new-1" : "subtask:page_ns_ch_blog_blog_panel_context:page:ns_ch_blog_blog:new-1",
                "task:node_view_panel_context_2:node_view:new-1" : "task:node_view_panel_context_2:node_view:new-1",
                "task:node_view_panel_context_2:node_view:new-5" : "task:node_view_panel_context_2:node_view:new-5",
                "task:node_view_panel_context_3:node_view:new-1" : "task:node_view_panel_context_3:node_view:new-1",
                "task:node_view_panel_context_3:node_view:new-2" : "task:node_view_panel_context_3:node_view:new-2",
                "task:node_view_panel_context_3:node_view:new-9" : "task:node_view_panel_context_3:node_view:new-9"
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
