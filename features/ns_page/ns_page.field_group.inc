<?php
/**
 * @file
 * ns_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ns_page_field_group_info() {
  $export = array();

  $field_group = new stdClass;
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_page_attachment|node|ns_page|form';
  $field_group->group_name = 'group_ns_page_attachment';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Attachments',
    'weight' => '4',
    'children' => array(
      0 => 'field_ns_page_link_list',
      1 => 'field_ns_page_attach_files',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_page_attachment|node|ns_page|form'] = $field_group;

  return $export;
}
