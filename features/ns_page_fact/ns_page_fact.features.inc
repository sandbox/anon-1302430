<?php
/**
 * @file
 * ns_page_fact.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ns_page_fact_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_node_info().
 */
function ns_page_fact_node_info() {
  $items = array(
    'ns_page_fact' => array(
      'name' => t('Page fact'),
      'base' => 'node_content',
      'description' => t('A page fact is a small gray box which can be added to a page sidebar. Usually these are used to further explain something mentioned on the page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
