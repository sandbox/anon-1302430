<?php
/**
 * @file
 * ns_poll.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function ns_poll_field_default_fields() {
  $fields = array();

  // Exported field: 'node-ns_article-field_ns_poll_ns_article_poll'
  $fields['node-ns_article-field_ns_poll_ns_article_poll'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_ns_poll_ns_article_poll',
      'foreign keys' => array(
        'nid' => array(
          'columns' => array(
            'nid' => 'nid',
          ),
          'table' => 'node',
        ),
      ),
      'indexes' => array(
        'nid' => array(
          0 => 'nid',
        ),
      ),
      'module' => 'node_reference',
      'settings' => array(
        'referenceable_types' => array(
          'ns_article' => 0,
          'ns_ch_web_promo' => 0,
          'ns_contributor' => 0,
          'ns_fact' => 0,
          'poll' => 'poll',
        ),
        'view' => array(
          'args' => array(),
          'display_name' => '',
          'view_name' => '',
        ),
      ),
      'translatable' => '1',
      'type' => 'node_reference',
    ),
    'field_instance' => array(
      'bundle' => 'ns_article',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'node_reference',
          'settings' => array(),
          'type' => 'node_reference_default',
          'weight' => '8',
        ),
        'search_index' => array(
          'label' => 'hidden',
          'module' => 'node_reference',
          'settings' => array(),
          'type' => 'node_reference_default',
          'weight' => '6',
        ),
        'search_result' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_ns_poll_ns_article_poll',
      'label' => 'Polls',
      'required' => 0,
      'settings' => array(
        'crossclone' => array(
          'couple_setting' => 0,
          'coupling' => 0,
          'delete_setting' => 0,
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'node_reference',
        'settings' => array(
          'autocomplete_match' => 'contains',
          'autocomplete_path' => 'node_reference/autocomplete',
          'references_dialog_add' => 1,
          'references_dialog_edit' => 1,
          'references_dialog_search' => 1,
          'references_dialog_search_view' => 'ns_article_reference_search:references_dialog_1',
          'size' => '60',
        ),
        'type' => 'node_reference_autocomplete',
        'weight' => '9',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Polls');

  return $fields;
}
