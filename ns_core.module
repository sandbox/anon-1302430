<?php
// UUIDs for our regions
define('REGION_ASIDE', '149a424b-72b4-1874-2559-57830f58194e');
define('MAIN_2_COL', '616385ed-713a-4764-5904-4a64ed03811e');
define('MAIN_3_COL', '999d1976-6850-afa4-b555-398d330431fa');
define('MAIN_1_COL', 'a3e30f46-72ed-e304-913c-3af5663d15f3');

/**
 * Implements of hook_theme().
 */
function ns_core_theme() {
  return array(
     // For our local tasks in the admin menu.
    'ns_core_local_tasks' => array(
      'variables' => array(),
      'file' => 'theme/theme.inc',
    ),
    // Our special page title, defined in the page title content pane.
    'ns_core_page_title' => array(
      'variables' => array('title' => NULL),
      'file' => 'theme/theme.inc',
    ),
    // Theme function for our custom date content type.
    'ns_core_node_date_custom' => array(
      'variables' => array('date' => NULL, 'title' => NULL, 'conf' => NULL),
      'file' => 'theme/theme.inc',
    ),
    'ns_core_today_date' => array(
      'variables' => array('date' => NULL, 'title' => NULL, 'conf' => NULL),
      'file' => 'theme/theme.inc',
    ),
  );
}

/**
 * Implements hook_permission().
 */
function ns_core_permission() {
  return array(
    'manage topic regions' => array(
      'title' => t('Manage topic regions'),
      'description' => t('Handle topic regions.'),
    ),
  );
}

/**
 * Implements of hook_ctools_plugin_directory().
 */
function ns_core_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements of hook_ctools_plugin_api().
 */
function ns_core_ctools_plugin_api($module, $api) {
  if ($module == 'dynamic_formatters' && $api == 'dynamic_formatters_default') {
    return array('version' => 1);
  }
}

/**
 * Wrapper function around filter_xss() with some additions.
 */
function ns_core_filter_xss($string, $allowed_tags = array('a', 'em', 'strong', 'cite', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd', 'br')) {
  return filter_xss($string, $allowed_tags);
}

/**
 * Implements hook_panels_pre_render().
 */
function ns_core_panels_pre_render(&$display, $renderer) {
  foreach ($display->content as $pane) {
    // Find the topic panel.
    if ($pane->subtype == 'ns_ch_web_topic-panel_pane_1') {
      // Fetch the tid, which should actually be a uuid,
      // and replace it with it's proper value.
      $tid = $pane->configuration['arguments']['tid_1'];
      if (module_exists('uuid_taxonomy') && !is_numeric($tid)) {
        $tid = uuid_taxonomy_term_find($tid);
      }
      // Add some css necessary for the "edit region" links.
      if (isset($pane->css['css_class'])) {
        $pane->css['css_class'] .= ' region-' . $tid;
      }
      else {
        $pane->css['css_class'] = ' region-' . $tid;
      }
    }
  }
}

/**
 * Implements hook_ctools_plugin_post_alter().
 */
function ns_core_ctools_plugin_post_alter(&$plugin, &$info) {
  // Override a function defined by the plugin.
  if (module_exists('uuid_taxonomy') && $plugin['module'] == 'views_content' && $info['type'] == 'content_types' && $plugin['content types'] == 'views_content_views_panes_content_type_content_types') {
    $plugin['edit form'] = 'ns_core_views_panes_edit_form';
    $plugin['render callback'] = 'ns_core_views_panes_render';
  }
}

/**
 * Custom render function that replaces uuids for tid arguments with a tid.
 */
function ns_core_views_panes_render($subtype, $conf, $panel_args, $contexts) {
  // Replace tids with uuids and then pass this on to the regular views content pane renderer.
  if ($subtype == 'ns_ch_web_topic-panel_pane_1' && !is_numeric($conf['arguments']['tid_1'])) {
    $conf['arguments']['tid_1'] = uuid_taxonomy_term_find($conf['arguments']['tid_1']);
  }
  return views_content_views_panes_content_type_render($subtype, $conf, $panel_args, $contexts);
}

/**
 * Custom form for views panes that replaces tid arguments with a select.
 */
function ns_core_views_panes_edit_form($form, &$form_state) {
  $form = views_content_views_panes_content_type_edit_form($form, $form_state);
  $conf = $form_state['conf'];
  $contexts = $form_state['contexts'];
  $pane = $form_state['pane'];
  // Add an autocomplete on the tid input.
  if ($pane->subtype == 'ns_ch_web_topic-panel_pane_1' && isset($form['arguments']['tid_1'])) {
    $form['arguments']['tid_1']['#type'] = 'select';
    $form['arguments']['tid_1']['#options'] = ns_core_get_regions();
  }
  $form['#submit'] = array('views_content_views_panes_content_type_edit_form_submit');
  return $form;
}

/**
 * Fetch uuids for regions
 * @param int $tid
 *   The taxonomy term id
 * @return string $uuid
 *   The uuid of the term.
 */
function ns_core_get_region_uuid($tid) {
  return db_select('taxonomy_term_data', 't')
    ->fields('t', array('uuid'))
    ->condition('tid', $tid)
    ->execute()
    ->fetchField();
}

/**
 * Get all regions keyed by uuid.
 */
function ns_core_get_regions() {
  $vids = taxonomy_vocabulary_get_names();
  return db_select('taxonomy_term_data', 't')
    ->addTag('translatable')
    ->addTag('term_access')
    ->fields('t', array('uuid', 'name'))
    ->condition('vid', $vids['ns_ch_web_topic_region']->vid)
    ->execute()
    ->fetchAllKeyed();
}

/**
 * Helper function for enabling panels everywhere.
 */
function ns_core_enable_panels_everywhere() {
  // Enable the site template.
  variable_set('panels_everywhere_site_template_enabled', 1);
  // Make sure the site template sample variant is disabled.
  ctools_include('export');
  $task = page_manager_get_task('site_template');
  $handlers = page_manager_load_task_handlers($task);
  if (isset($handlers['site_template_panel_context_basic'])) {
    $handler = $handlers['site_template_panel_context_basic'];
    ctools_export_set_object_status($handler, TRUE);
  }
}

/**
 * Helper function for enabling panels node view.
 */
function ns_core_enable_panels_node_view() {
  variable_set('page_manager_node_view_disabled', 0);
  variable_set('workbench_panels_node_draft_disabled', 0);
  variable_set('workbench_panels_node_revision_disabled', 0);
}

/**
 * Helper function for enabling panels term view.
 */
function ns_core_enable_panels_term_view() {
  variable_set('page_manager_term_view_disabled', 0);
}

/**
 * Set the enabled view modes for a field bundle.
 * @param string $entity_type
 *   The entity type.
 * @param string $bundle
 *   The bundle
 * @param string $enabled_view_modes
 *   The view modes that should be enabled. All other will have it's custom
 *   display settings disabled.
 */
function ns_core_set_bundle_enabled_view_modes($entity_type, $bundle, $enabled_view_modes) {
  // Check that the specified entity type and bundle exists, and bail if they don't
  $settings = field_bundle_settings($entity_type, $bundle);
  $view_mode_settings = field_view_mode_settings($entity_type, $bundle);
  $settings['view_modes'] = array();
  foreach ($enabled_view_modes as $view_mode) {
    $settings['view_modes'][$view_mode]['custom_settings'] = 1;
  }
  field_bundle_settings($entity_type, $bundle, $settings);
}
